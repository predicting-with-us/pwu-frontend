import React from "react";
import { Line } from "react-chartjs-2";
import styled from "styled-components";

const Container = styled.div`
  height: 545px;
`;

const LineChart = (props: any) => {
  return (
    <Container>
      <h2>Prediction Chart</h2>
      <Line data={props} />
    </Container>
  );
};

export default LineChart;
