import React, { useState } from "react";
import { FileInput } from "@blueprintjs/core";

export interface FileUploaderPropsType {
  setData: (arg0: number[]) => void;
}

const FileUploader = (props: FileUploaderPropsType) => {
  const { setData } = props;

  const onChangeHandler = async (event: any) => {
    const file = event.target.files[0] as Blob;
    processFile(file);
  };

  const processFile = (file: Blob) => {
    const reader = new FileReader();
    reader.onload = () => {
      const textFile = reader.result as string;
      const result = textFile.split("\n").map(row => +row);
      setData(result);
    };
    reader.readAsText(file);
  };

  return (
    <div>
      <FileInput text={"Upload CSV File"} onChange={onChangeHandler} />
    </div>
  );
};

export default FileUploader;
