import React, { useState } from "react";
import styled from "styled-components";

import InputRow from "./InputRow";
import { Button, Intent } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

const Container = styled.div`
  height: 470px;
  overflow-y: scroll;
`;

const addButtonStyle = {
  width: "90%",
  marginTop: "10px",
  marginLeft: "8px"
};

export interface InputPanePropsType {
  data: number[];
  setData: (arg0: number[]) => void;
}

export default function InputPane(props: InputPanePropsType) {
  const { data, setData } = props;

  const addRow = () => setData([...data, 0]);

  const removeRow = (i: number) => setData([...data.slice(0, i), ...data.slice(i + 1, data.length)]);

  const changeData = (i: number, value: number) =>

    setData([...data.slice(0, i), isNaN(value) ? -1 : value, ...data.slice(i + 1, data.length)]);

  const renderPanel = () =>
    data.map((value, i) => (
      <InputRow
        number={i}
        value={value}
        onRemove={() => removeRow(i)}
        onChange={(value: number) => changeData(i, value)}
      />
    ));

  return (
    <>
      <h2>Input Data:</h2>
      <Container>
        {renderPanel()}
        <Button style={addButtonStyle} onClick={addRow} icon={IconNames.PLUS}>
          Add row
        </Button>
      </Container>
    </>
  );
}
