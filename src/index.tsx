import React, { useState } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Button, Intent } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import LineChart from "./LineChart";
import * as serviceWorker from "./serviceWorker";
import FileUploader from "./FileUpload";
import InputPane from "./InputPane";

import "./index.css";

const LeftSide = styled.div`
  width: 370px;
  height: 100%;
`;

const RightSide = styled.div`
  margin-left: 20px;
  width: 100%;
`;

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin: 10px;
`;

const ChartContainer = styled.div`
  font-family: sans-serif;
  text-align: center;
  width: 100%;
`;

const predictbuttonStyle = {
  width: "100%",
  marginTop: "10px"
};

const initData = {
  labels: ["", "", "", ""],
  datasets: [
    {
      label: "prediction",
      fill: false,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(0,192,192,1)",
      borderDash: [],
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      data: [0, 0, 0, 0]
    },
    {
      label: "data",
      fill: true,
      showLine: false,
      backgroundColor: "red",
      borderColor: "red",
      borderDash: [],
      borderJoinStyle: "miter",
      pointBorderColor: "red",
      pointBackgroundColor: "red",
      pointRadius: 6,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      data: [0, 0, 0, 0]
    }
  ]
};

const App = () => {
  const [dataToRender, setDataToRender] = useState(initData);
  const [predictionValue, setPredictionValue] = useState(-1);
  const [data, setData] = useState([100, 200, 300, 400]);

  const getFormula = async () => {
    const url = process.env.NODE_ENV === "production" ? "http://35.197.134.5" : "http://localhost:3001";
    const response = await fetch(url+"/lsp", {
      method: "POST",
      body: JSON.stringify({ values: data })
    });
    const formula = (await response.json()).values;

    return formula;
  };

  const renderChart = async () => {
    const formula = await getFormula();
    const dataPredict: number[] = [];
    const label: string[] = [];
    const dataRaw: number[] = [];

    [...data, undefined].forEach((point: number | undefined, i: number) => {
      const total = formula
        .map((x: number, j: number) => x * Math.pow(i, j))
        .reduce((a: number, b: number) => a + b, 0);
      dataPredict.push(total);
      label.push("" + (i + 1));
      if (point !== undefined) dataRaw.push(point);
    });

    const newData = { ...initData };
    newData.datasets[0].data = dataPredict;
    newData.datasets[1].data = dataRaw;
    newData.labels = label;
    setDataToRender(newData);
    const tempPredictionValue = dataPredict[dataPredict.length - 1];
    setPredictionValue(Number((tempPredictionValue).toFixed(2)));
  };

  return (
    <Container>
      <LeftSide>
        <InputPane data={data} setData={setData} />
        <FileUploader setData={setData} />
        <Button
          style={predictbuttonStyle}
          rightIcon={IconNames.ARROW_RIGHT}
          onClick={renderChart}
          intent={Intent.PRIMARY}
        >
          Predict
        </Button>
      </LeftSide>
      <RightSide>
        <ChartContainer>
          {LineChart(dataToRender)}
          <h3>
            Next Prediction Value:{" "}
            <strong>
              <u>{predictionValue}</u>
            </strong>
          </h3>
        </ChartContainer>
      </RightSide>
    </Container>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
