import React from "react";
import styled from "styled-components";
import { NumericInput, Button, Intent } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 10px;
`;

const NumberSection = styled.span`
  width: 10px;
`;

const inputFormStyle = {
  marginLeft: "20px",
  width: "200px"
};

const removeButtonStyle = {
  marginLeft: "3px"
};

function InputRow(props: any) {
  const { number, value, onRemove, onChange } = props;

  return (
    <Container>
      <NumberSection>{number + 1}</NumberSection>:
      <NumericInput
        style={inputFormStyle}
        buttonPosition={"none"}
        placeholder="Data"
        value={value}
        onValueChange={onChange}
      />
      <Button style={removeButtonStyle} intent={Intent.DANGER} onClick={onRemove} icon={IconNames.CROSS}></Button>
    </Container>
  );
}

export default InputRow;
