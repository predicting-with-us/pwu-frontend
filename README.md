# Predicting With Us - Frontend

## How to run at Local
- Install node package manager
- Clone this repository
- Open terminal/cmd in root directory
- Run `npm install`
- Run `npm start`
- Check on browser `http://127.0.0.1:3000`

## Live version
[https://predicting-with-us.netlify.com](https://predicting-with-us.netlify.com)
